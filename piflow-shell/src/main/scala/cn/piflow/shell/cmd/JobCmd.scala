package cn.piflow.shell.cmd

import cn.piflow.shell.Cmd
import cn.piflow.util.FormatUtils
import cn.piflow.{FlowEngine, JobInstance, ScheduledJob}

import scala.collection.Seq

class JobCmd(engine: FlowEngine) extends Cmd {
	val jobs = engine.getJobService();

	def list = listJobInstances(None);

	def list(jobId: String): Unit = listJobInstances(Some(jobId));

	def list(jobId: Int): Unit = list("" + jobId);

	def slist(): Unit = listScheduledJobs();

	def hist(): Unit = listJobExecutions();

	def hist(jobId: String): Unit = listJobExecutions(jobId);

	def hist(jobId: Int): Unit = hist("" + jobId);

	private def existingJobId(jobId: String): Option[String] = {
		if (!jobs.exists(jobId)) {
			println(s"invalid job id: $jobId");
			None;
		}
		else
			Some(jobId);
	}

	def kill(jobId: String) = {
		existingJobId(jobId).foreach { jobId ⇒
			jobs.stop(jobId);
			println(s"job killed: id=$jobId");
		}
	}

	def pause(jobId: String) = {
		existingJobId(jobId).foreach { jobId ⇒
			jobs.pause(jobId);
			println(s"job paused: id=$jobId");
		}
	}

	def resume(jobId: String) = {
		existingJobId(jobId).foreach { jobId ⇒
			jobs.resume(jobId);
			println(s"job paused: id=$jobId");
		}
	}

	private def printInstances(jobs: Seq[JobInstance]) = {
		val data = jobs.map { ji: JobInstance ⇒
			val sj = ji.getScheduledJob();
			Seq(ji.getId(), sj.getId(), FormatUtils.format(ji.getStartTime()), ji.getRunTime());
		}

		FormatUtils.printTable(Seq("id", "sid", "start-time", "run-time"), data, "");
	}

	def listJobInstances(jobId: Option[String]) = {
		if (jobId.isDefined) {
			existingJobId(jobId.get).foreach { jobId ⇒
				printInstances(jobs.getRunningJobs(jobId));
			}
		}
		else {
			printInstances(jobs.getRunningJobs());
		};
	}

	def listJobExecutions(jobId: String) = {
		printInstances(jobs.getHistoricExecutions(jobId));
	}

	def listJobExecutions() = {
		printInstances(jobs.getHistoricExecutions());
	}

	def listScheduledJobs() = {
		val data = jobs.getScheduledJobs().map { sj: ScheduledJob ⇒
			Seq(sj.getId(), FormatUtils.format(sj.getStartTime()),
				jobs.getFireCount(sj.getId()), FormatUtils.format(sj.getPreviousFireTime()),
				FormatUtils.format(sj.getNextFireTime()));
		}

		FormatUtils.printTable(Seq("sid", "start-time", "run-times", "previous-fire", "next-fire"), data, "");
	}
}