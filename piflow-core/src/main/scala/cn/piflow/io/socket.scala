package cn.piflow.io

import cn.piflow.JobContext
import org.apache.spark.sql.execution.streaming.TextSocketSource

/**
	* Created by bluejoe on 2017/10/10.
	*/
case class SocketStreamSource(host: String, port: Int, includeTimestamp: Boolean = false)
	extends SparkStreamSourceAdapter {

	def createSparkStreamSource(ctx: JobContext) =
		new TextSocketSource(host, port, includeTimestamp, ctx.sqlContext);
}
